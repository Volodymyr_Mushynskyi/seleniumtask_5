package com.epam.po;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static com.epam.utils.DriverManager.getWebDriver;


public class GmailLoginPage {

  private static final Logger logger = LogManager.getLogger(GmailLoginPage.class);
  private WebDriverWait wait = new WebDriverWait(getWebDriver(), 30);

  @FindBy(xpath = "//input[@type='email']")
  private WebElement emailInputField;

  @FindBy(xpath = "//div[@class='Xb9hP']//input[@type='password']")
  private WebElement passwordInputField;

  @FindBy(id = "identifierNext")
  private WebElement emailNextButton;

  @FindBy(id = "passwordNext")
  private WebElement passwordNextButton;

  public GmailLoginPage() {
    PageFactory.initElements(getWebDriver(), this);
  }

  public GmailLoginPage fillEmailInputField(String email) {
    logger.info("Fill Email input field");
    wait.until(ExpectedConditions.elementToBeClickable(emailInputField));
    emailInputField.sendKeys(email);
    return this;
  }

  public GmailLoginPage fillPasswordInputField(String password) {
    logger.info("Fill Password input field");
    wait.until(ExpectedConditions.elementToBeClickable(passwordInputField));
    passwordInputField.sendKeys(password);
    return this;
  }

  public GmailLoginPage clickByEmailNextButton() {
    logger.info("Click by next button");
    emailNextButton.click();
    return this;
  }

  public GmailLoginPage clickByPasswordNextButton() {
    logger.info("Click by next button");
    passwordNextButton.click();
    return this;
  }
}
